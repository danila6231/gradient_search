import sys

import numpy as np
from matplotlib import pyplot as plt


def scan_image(path: str) -> np.ndarray:
    image = plt.imread(path)[:, :, :3]
    return image


def calc_grad(i, j, matrix):
    up = i, j
    down = i, j
    left = i, j
    right = i, j
    if i + 1 != matrix.shape[0]:
        up = i + 1, j
    if i - 1 != -1:
        down = i - 1, j
    if j + 1 != matrix.shape[1]:
        right = i, j + 1
    if j - 1 != -1:
        left = i, j - 1

    vectors = [
        matrix[up] - matrix[i, j],
        matrix[down] - matrix[i, j],
        matrix[right] - matrix[i, j],
        matrix[left] - matrix[i, j]
    ]

    ind = np.argmax(map(np.linalg.norm, vectors))

    return vectors[ind]


def get_gradients(matrix: np.ndarray) -> np.ndarray:
    grads = np.zeros(matrix.shape)
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            grads[i][j][:] = calc_grad(i, j, matrix)
    return grads


def hightlight_grad(matrix: np.ndarray):
    gradients = get_gradients(matrix)
    plt.imsave(sys.argv[2], abs(gradients[:, :, 2]) / 255)


def main():
    image = sys.argv[1]
    matrix = scan_image(image)
    hightlight_grad(matrix)


if __name__ == '__main__':
    main()
